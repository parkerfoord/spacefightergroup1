#pragma once
#include "GameObject.h"
#include "KatanaEngine.h"
#include "EnemyShip.h"
#include "Font.h"
#include <conio.h>


using namespace KatanaEngine;

class Score : public GameObject {
private:
	static int m_score;

public:
	static void IncramentScore(int increase);

	static void SetFont(Font *pFont);

	static Font *m_scoreFont;
	
	static int GetScore() { return m_score; }

	static void Draw(SpriteBatch *pSpriteBatch);

	static std::string m_ScoreString;

protected:
	std::string ScoreString;
};





