#include "Score.h"
#include <string>
#include <sstream>

//Aparently, a static variable in an h file must be declared again in the cpp file
int Score::m_score = 0;
Font* Score::m_scoreFont = nullptr;
std::string Score::m_ScoreString;

void Score::IncramentScore(int increase) {
	m_score += increase;
}

void Score::SetFont(Font *pFont) {
	m_scoreFont = pFont;
}

void Score::Draw(SpriteBatch *pSpriteBatch)
{
	std::stringstream ss;
	ss << Score::GetScore();
	m_ScoreString = "Score: " + ss.str();

	pSpriteBatch->DrawString(m_scoreFont, &m_ScoreString, Vector2(Game::GetScreenWidth() - 10, 10), Color::White, TextAlign::RIGHT, 1);
}